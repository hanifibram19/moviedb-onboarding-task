/**
 * Copyright (C) 2018 Fernando Cejas Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hanifibram.moviedb.core.navigation

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v4.app.FragmentActivity
import android.view.View
import com.hanifibram.moviedb.features.login.Authenticator
import com.hanifibram.moviedb.features.movies.Movie
import com.hanifibram.moviedb.features.movies.MovieDetailActivity
import com.hanifibram.moviedb.features.movies.MoviesActivity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Navigator
@Inject constructor(private val authenticator: Authenticator) {

    fun showMovies(context: Context) = context.startActivity(MoviesActivity.callingIntent(context))

    fun showDetailMovie(activity: FragmentActivity, movie: Movie) =
            activity.startActivity(MovieDetailActivity.callingIntent(activity, movie))

    fun youTubeIntent(context: Context, youtubeKey: String) {
        var youtubeIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$youtubeKey"))
        if (youtubeIntent.resolveActivity(context.packageManager) == null)
            youtubeIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=$youtubeKey"))

        context.startActivity(youtubeIntent)
    }

    class Extras(val transitionSharedElement: View)
}


