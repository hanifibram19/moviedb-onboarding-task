package com.hanifibram.moviedb.features.movies


import com.google.gson.annotations.SerializedName

data class TrailerWrapper(
        @SerializedName("id")
        val id: Int,
        @SerializedName("results")
        val results: List<Trailer>
) {
    companion object {
        fun empty() = TrailerWrapper(0, emptyList())
    }
}