package com.hanifibram.moviedb.features.movies


import com.google.gson.annotations.SerializedName

data class MovieDetail(
        @SerializedName("backdrop_path")
        val backdropPath: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("poster_path")
        val posterPath: String,
        @SerializedName("runtime")
        val runtime: Int?,
        @SerializedName("release_date")
        val releaseDate: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("tagline")
        val tagline: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("video")
        val video: Boolean,
        @SerializedName("vote_average")
        val voteAverage: Double,
        @SerializedName("vote_count")
        val voteCount: Int
) {
    companion object {
        fun empty() = MovieDetail("", 0, "", "", 0, "", "", "", "", false, 0.0, 0)
    }
}