package com.hanifibram.moviedb.features.movies

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hanifibram.moviedb.R
import com.hanifibram.moviedb.core.extension.inflate
import kotlinx.android.synthetic.main.trailer.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class TrailerAdapter
@Inject constructor() : RecyclerView.Adapter<TrailerAdapter.ViewHolder>() {

    internal var collection: List<Trailer> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (Trailer) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent.inflate(R.layout.trailer))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(collection[position], clickListener)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(trailer: Trailer, clickListener: (Trailer) -> Unit) {
            itemView.setOnClickListener { clickListener(trailer) }
            itemView.trailerName.text = trailer.name
        }
    }

}