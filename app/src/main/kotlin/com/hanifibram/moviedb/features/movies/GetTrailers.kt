package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class GetTrailers
@Inject constructor(private val repository: MoviesRepository) : UseCase<TrailerWrapper, Int>() {
    override suspend fun run(params: Int): Either<Failure, TrailerWrapper> =
            repository.getTrailerMovie(params)

}