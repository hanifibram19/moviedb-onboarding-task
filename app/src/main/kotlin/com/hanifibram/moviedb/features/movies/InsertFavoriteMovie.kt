package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class InsertFavoriteMovie
@Inject constructor(private val moviesRepository: MoviesRepository) : UseCase<UseCase.None, Movie>() {
    override suspend fun run(params: Movie): Either<Failure, None> =
            moviesRepository.insertFavoriteMovie(params)
}