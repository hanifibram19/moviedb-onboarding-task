package com.hanifibram.moviedb.features.movies

import android.os.Bundle
import android.view.View
import com.hanifibram.moviedb.R
import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.extension.failure
import com.hanifibram.moviedb.core.extension.loadFromUrl
import com.hanifibram.moviedb.core.extension.observe
import com.hanifibram.moviedb.core.extension.viewModel
import com.hanifibram.moviedb.core.navigation.Navigator
import com.hanifibram.moviedb.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_movie_detail.*
import java.text.SimpleDateFormat
import javax.inject.Inject

class MovieDetailFragment : BaseFragment() {

    override fun layoutId(): Int = R.layout.fragment_movie_detail

    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var movieDetailViewModel: MovieDetailViewModel
    @Inject
    lateinit var trailerAdapter: TrailerAdapter

    private lateinit var movie: Movie

    companion object {
        private const val ARG_MOVIE = "arg-movie"

        fun forInstance(movie: Movie): MovieDetailFragment {
            val movieDetailFragment = MovieDetailFragment()
            val arguments = Bundle()
            arguments.putParcelable(ARG_MOVIE, movie)
            movieDetailFragment.arguments = arguments
            return movieDetailFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        appComponent.inject(this)

        movieDetailViewModel = viewModel(viewModelFactory) {
            observe(movieDetail, ::handleMovieDetail)
            failure(failure, ::handleFailureMovieDetail)

            observe(trailers, ::handleTrailerList)
            failure(failure, ::handleFailureTrailerList)

            observe(movie, ::handleFavoriteMovie)
            failure(failure, ::handleFailureFavoriteMovie)

            observe(addFavoriteMovie, ::handleInsertMovie)
            failure(failure, ::handleFailureInsertMovie)

            observe(removeFavoriteMovie, ::handleDeleteFavorite)
            failure(failure, ::handleFailureDeleteFavorite)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (firstTimeCreated(savedInstanceState)) {
            movie = arguments?.get(ARG_MOVIE) as Movie
            movieDetailViewModel.loadMoviesDetail(movie.id)
            movieDetailViewModel.loadTrailers(movie.id)
        }

        initView()
    }

    private fun initView() {
        trailers.adapter = trailerAdapter
        trailerAdapter.clickListener = { trailer ->
            movieDetailViewModel.openYoutube(trailer.key)
        }

    }

    private fun handleTrailerList(trailerList: List<Trailer>?) {
        trailerAdapter.collection = trailerList.orEmpty()
    }

    private fun handleFailureTrailerList(failure: Failure?) {
        println(failure)
    }

    private fun handleMovieDetail(movieDetail: MovieDetail?) {
        movieDetail?.let {
            with(movieDetail) {
                // format date
                val date = SimpleDateFormat("yyyy-MM-dd").parse(releaseDate)
                val formatDate = SimpleDateFormat("MMM dd, yyyy").format(date)

                movieDetailTitle.text = title
                movieOverview.text = overview
                movieRate.text = "${voteAverage}/10"
                movieReleaseDate.text = formatDate
                duration.text = "${runtime}min"
                movieDetailPoster.loadFromUrl("http://image.tmdb.org/t/p/w500/${posterPath}")

                switchFavorite.setOnCheckedChangeListener { compoundButton, checked ->
                    if (compoundButton.isPressed) {
                        if (checked)
                            movieDetailViewModel.insertFavoriteMovie(movie)
                        else
                            movieDetailViewModel.deleteFavoriteMovie(movie.id)
                    }
                }
            }
        }

        movieDetailViewModel.getFavoriteMovie(movie.id)
    }

    private fun handleFailureMovieDetail(failure: Failure?) {
        println(failure)
    }

    private fun handleInsertMovie(b: Boolean?) {
        notify(R.string.movie_notify_success_insert_favorite)
    }

    private fun handleFailureInsertMovie(f: Failure?) {
    }

    private fun handleFavoriteMovie(m: Movie?) {
        switchFavorite.isChecked = true
    }

    private fun handleFailureFavoriteMovie(failure: Failure?) {
        println(failure)
    }

    private fun handleDeleteFavorite(b: Boolean?) {
        switchFavorite.isChecked = false
    }

    private fun handleFailureDeleteFavorite(failure: Failure?) {

    }


}