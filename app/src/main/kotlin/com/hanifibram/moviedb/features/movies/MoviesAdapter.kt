package com.hanifibram.moviedb.features.movies

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.hanifibram.moviedb.R
import com.hanifibram.moviedb.core.extension.inflate
import com.hanifibram.moviedb.core.extension.loadFromUrl
import kotlinx.android.synthetic.main.movie.view.*
import javax.inject.Inject
import kotlin.properties.Delegates

class MoviesAdapter @Inject constructor() : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {

    internal var collection: List<Movie> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    internal var clickListener: (Movie) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
            ViewHolder(parent.inflate(R.layout.movie))

    override fun getItemCount(): Int = collection.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
            holder.bind(collection[position], clickListener)

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(movie: Movie, clickListener : (Movie) -> Unit ) {
            itemView.poster.loadFromUrl("http://image.tmdb.org/t/p/w500/${movie.posterPath}")
            itemView.setOnClickListener { clickListener(movie) }
        }
    }

}