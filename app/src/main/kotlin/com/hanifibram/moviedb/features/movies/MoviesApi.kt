package com.hanifibram.moviedb.features.movies

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

internal interface MoviesApi {

    @GET("movie/popular")
    fun getPopularMovie(): Call<MovieWrapper>

    @GET("movie/top_rated")
    fun getTopRatedMovie(): Call<MovieWrapper>

    @GET("movie/{id}")
    fun getDetailMovie(@Path("id") id: Int): Call<MovieDetail>

    @GET("movie/{id}/videos")
    fun getTrailerMovie(@Path("id") id: Int): Call<TrailerWrapper>

}