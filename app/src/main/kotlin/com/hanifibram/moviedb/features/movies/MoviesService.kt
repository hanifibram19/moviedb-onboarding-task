package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.database.AppDatabase
import com.hanifibram.moviedb.core.interactor.UseCase
import retrofit2.Call
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MoviesService
@Inject constructor(retrofit: Retrofit, appDatabase: AppDatabase) : MoviesApi, MovieDao {
    private val movies2Api by lazy { retrofit.create(MoviesApi::class.java) }
    private val movieDao = appDatabase.movieDao()

    override fun getPopularMovie(): Call<MovieWrapper> = movies2Api.getPopularMovie()
    override fun getTopRatedMovie(): Call<MovieWrapper> = movies2Api.getTopRatedMovie()
    override fun getDetailMovie(id: Int): Call<MovieDetail> = movies2Api.getDetailMovie(id)
    override fun getTrailerMovie(id: Int): Call<TrailerWrapper> = movies2Api.getTrailerMovie(id)

    override suspend fun insert(movie: Movie) = movieDao.insert(movie)
    override fun getAllFavoriteMovie(): List<Movie> = movieDao.getAllFavoriteMovie()
    override fun getFavoriteMovie(id: Int): Movie? = movieDao.getFavoriteMovie(id)
    override suspend fun deleteFavoriteMovie(id: Int) = movieDao.deleteFavoriteMovie(id)
}