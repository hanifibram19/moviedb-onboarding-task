package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class GetTopRatedMovie
@Inject constructor(private val moviesRepository: MoviesRepository) : UseCase<MovieWrapper, UseCase.None>() {
    override suspend fun run(params: None): Either<Failure, MovieWrapper> = moviesRepository.getTopRatedMovies()
}