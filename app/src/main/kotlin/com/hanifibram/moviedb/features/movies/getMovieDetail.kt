package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class GetMovieDetail
@Inject constructor(private val moviesRepository: MoviesRepository) : UseCase<MovieDetail, Int>() {
    override suspend fun run(params: Int): Either<Failure, MovieDetail> =
            moviesRepository.getDetailMovies(params)
}