package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class GetFavoriteMovie
@Inject constructor(private val moviesRepository: MoviesRepository) : UseCase<Movie, Int>() {
    override suspend fun run(params: Int): Either<Failure, Movie> =
            moviesRepository.getFavoriteMovie(params)
}