package com.hanifibram.moviedb.features.movies

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.hanifibram.moviedb.core.platform.KParcelable

@Entity(tableName = "movie_favorite")
data class Movie(
        @ColumnInfo(name = "adult")
        @SerializedName("adult")
        val adult: Boolean,
        @ColumnInfo(name = "backdrop_path")
        @SerializedName("backdrop_path")
        val backdropPath: String,
        @PrimaryKey
        @ColumnInfo(name = "id")
        @SerializedName("id")
        val id: Int,
        @ColumnInfo(name = "original_language")
        @SerializedName("original_language")
        val originalLanguage: String,
        @ColumnInfo(name = "original_title")
        @SerializedName("original_title")
        val originalTitle: String,
        @ColumnInfo(name = "overview")
        @SerializedName("overview")
        val overview: String,
        @ColumnInfo(name = "popularity")
        @SerializedName("popularity")
        val popularity: Double,
        @ColumnInfo(name = "poster_path")
        @SerializedName("poster_path")
        val posterPath: String,
        @ColumnInfo(name = "release_date")
        @SerializedName("release_date")
        val releaseDate: String,
        @ColumnInfo(name = "title")
        @SerializedName("title")
        val title: String,
        @ColumnInfo(name = "video")
        @SerializedName("video")
        val video: Boolean,
        @ColumnInfo(name = "vote_average")
        @SerializedName("vote_average")
        val voteAverage: Double,
        @ColumnInfo(name = "vote_count")
        @SerializedName("vote_count")
        val voteCount: Int
) : KParcelable {
    constructor(parcel: Parcel) : this(
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readDouble(),
            parcel.readInt())

    override fun writeToParcel(dest: Parcel, flags: Int) {
        with(dest) {
            writeByte(if (adult) 1 else 0)
            writeString(backdropPath)
            writeInt(id)
            writeString(originalLanguage)
            writeString(originalTitle)
            writeString(overview)
            writeDouble(popularity)
            writeString(posterPath)
            writeString(releaseDate)
            writeString(title)
            writeByte(if (video) 1 else 0)
            writeDouble(voteAverage)
            writeInt(voteCount)
        }
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Movie> {
        override fun createFromParcel(parcel: Parcel): Movie {
            return Movie(parcel)
        }

        override fun newArray(size: Int): Array<Movie?> {
            return arrayOfNulls(size)
        }
    }
}