package com.hanifibram.moviedb.features.movies

import android.arch.lifecycle.MutableLiveData
import com.hanifibram.moviedb.core.interactor.UseCase
import com.hanifibram.moviedb.core.platform.BaseViewModel
import javax.inject.Inject

class MoviesViewModel
@Inject constructor(private val getMovies2: GetMovies,
                    private val getAllFavoriteMovie: GetAllFavoriteMovie,
                    private val getTopRatedMovie: GetTopRatedMovie) : BaseViewModel() {

    var movies: MutableLiveData<List<Movie>> = MutableLiveData()
    var typeOfListMovies: MutableLiveData<Int> = MutableLiveData()

    fun loadPopularMovie() = getMovies2(UseCase.None()) { it.fold(::handleFailure, ::handleMovieList) }

    fun loadTopRatedMovie() = getTopRatedMovie(UseCase.None()) { it.fold(::handleFailure, ::handleMovieList) }

    fun loadFavoriteMovie() =
            getAllFavoriteMovie(UseCase.None()) { it.fold(::handleFailure, ::handleFavoriteMovieList) }

    private fun handleMovieList(movie: MovieWrapper) {
        this.movies.value = movie.results
    }

    private fun handleFavoriteMovieList(movies: List<Movie>) {
        this.movies.value = movies
    }


}