package com.hanifibram.moviedb.features.movies

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hanifibram.moviedb.core.interactor.UseCase

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(movie: Movie)

    @Query("SELECT * FROM movie_favorite")
    fun getAllFavoriteMovie(): List<Movie>

    @Query("SELECT * FROM movie_favorite WHERE id= :id")
    fun getFavoriteMovie(id: Int): Movie?

    @Query("DELETE FROM movie_favorite WHERE id= :id")
    suspend fun deleteFavoriteMovie(id: Int)

}