package com.hanifibram.moviedb.features.movies

import android.os.Bundle
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import com.hanifibram.moviedb.R
import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.extension.failure
import com.hanifibram.moviedb.core.extension.observe
import com.hanifibram.moviedb.core.extension.viewModel
import com.hanifibram.moviedb.core.navigation.Navigator
import com.hanifibram.moviedb.core.platform.BaseFragment
import kotlinx.android.synthetic.main.fragment_movies.*
import javax.inject.Inject

const val MOVIES_POPULAR = 0
const val MOVIES_FAVORITE = 1
const val MOVIES_TOP_RATED = 2

class MoviesFragment : BaseFragment() {

    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var movies2ViewModel: MoviesViewModel
    @Inject
    lateinit var moviesAdapter: MoviesAdapter

    override fun layoutId() = R.layout.fragment_movies

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appComponent.inject(this)

        movies2ViewModel = viewModel(viewModelFactory) {
            observe(movies, ::renderMoviesList)
            failure(failure, ::handleFailure)
        }

        // default value
        movies2ViewModel.typeOfListMovies.value = MOVIES_POPULAR
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        setHasOptionsMenu(true)
    }

    override fun onResume() {
        super.onResume()

        when (movies2ViewModel.typeOfListMovies.value) {
            MOVIES_POPULAR -> movies2ViewModel.loadPopularMovie()
            MOVIES_FAVORITE -> movies2ViewModel.loadFavoriteMovie()
            MOVIES_TOP_RATED -> movies2ViewModel.loadTopRatedMovie()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.movies_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.menu_popular -> {
                movies2ViewModel.loadPopularMovie()
                movies2ViewModel.typeOfListMovies.value = MOVIES_POPULAR
            }
            R.id.menu_favorite -> {
                movies2ViewModel.loadFavoriteMovie()
                movies2ViewModel.typeOfListMovies.value = MOVIES_FAVORITE
            }
            R.id.menu_top_rated -> {
                movies2ViewModel.loadTopRatedMovie()
                movies2ViewModel.typeOfListMovies.value = MOVIES_TOP_RATED
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initView() {
        movies.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        movies.adapter = moviesAdapter
        moviesAdapter.clickListener = { movie -> navigator.showDetailMovie(activity!!, movie) }
    }

    private fun renderMoviesList(movies: List<Movie>?) {
        moviesAdapter.collection = movies.orEmpty()
    }

    private fun handleFailure(failure: Failure?) {
    }

}