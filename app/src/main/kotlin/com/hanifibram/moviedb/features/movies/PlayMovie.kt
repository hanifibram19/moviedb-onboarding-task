package com.hanifibram.moviedb.features.movies

import android.content.Context
import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import com.hanifibram.moviedb.core.navigation.Navigator
import javax.inject.Inject

class PlayMovie
@Inject constructor(private val context: Context, private val navigator: Navigator)
    : UseCase<UseCase.None, String>() {
    override suspend fun run(params: String): Either<Failure, None> {
        navigator.youTubeIntent(context, params)
        return Either.Right(None())
    }
}