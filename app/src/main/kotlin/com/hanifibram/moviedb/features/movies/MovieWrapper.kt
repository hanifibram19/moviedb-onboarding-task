package com.hanifibram.moviedb.features.movies
import com.google.gson.annotations.SerializedName


data class MovieWrapper(
    @SerializedName("page")
    val page: Int,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int,
    @SerializedName("results")
    val results: List<Movie>
){
    companion object{
        fun empty() = MovieWrapper(0, 0, 0, emptyList())
    }
}