package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import com.hanifibram.moviedb.core.platform.NetworkHandler
import retrofit2.Call
import javax.inject.Inject

interface MoviesRepository {
    fun getPopularMovies(): Either<Failure, MovieWrapper>
    fun getTopRatedMovies(): Either<Failure, MovieWrapper>
    fun getDetailMovies(id: Int): Either<Failure, MovieDetail>
    fun getTrailerMovie(id: Int): Either<Failure, TrailerWrapper>
    fun getAllFavoriteMovie(): Either<Failure, List<Movie>>
    suspend fun insertFavoriteMovie(movie: Movie): Either<Failure, UseCase.None>
    fun getFavoriteMovie(id: Int): Either<Failure, Movie>
    suspend fun deleteFavorite(id: Int): Either<Failure, UseCase.None>

    class MoviesRepositoryImpl
    @Inject constructor(private val networkHandler: NetworkHandler,
                        private val service: MoviesService) : MoviesRepository {

        override fun getPopularMovies(): Either<Failure, MovieWrapper> {
            return when (networkHandler.isConnected) {
                true -> request(service.getPopularMovie(), { it }, MovieWrapper.empty())
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }

        override fun getTopRatedMovies(): Either<Failure, MovieWrapper> {
            return when (networkHandler.isConnected) {
                true -> request(service.getTopRatedMovie(), { it }, MovieWrapper.empty())
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }

        override fun getDetailMovies(id: Int): Either<Failure, MovieDetail> {
            return when (networkHandler.isConnected) {
                true -> request(service.getDetailMovie(id), { it }, MovieDetail.empty())
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }

        override fun getTrailerMovie(id: Int): Either<Failure, TrailerWrapper> {
            return when (networkHandler.isConnected) {
                true -> request(service.getTrailerMovie(id), { it }, TrailerWrapper.empty())
                false, null -> Either.Left(Failure.NetworkConnection)
            }
        }

        override fun getAllFavoriteMovie(): Either<Failure, List<Movie>> {
            return Either.Right(service.getAllFavoriteMovie())
        }

        override suspend fun insertFavoriteMovie(movie: Movie): Either<Failure, UseCase.None> {
            service.insert(movie)
            return Either.Right(UseCase.None())
        }

        override fun getFavoriteMovie(id: Int): Either<Failure, Movie> {
            val movie = service.getFavoriteMovie(id)
            return when (movie != null) {
                true -> Either.Right(movie!!)
                false -> Either.Left(Failure.DatabaseError)
            }
        }

        override suspend fun deleteFavorite(id: Int): Either<Failure, UseCase.None> {
            val movie = service.getFavoriteMovie(id)
            return when (movie != null) {
                true -> {
                    service.deleteFavoriteMovie(id)
                    Either.Right(UseCase.None())
                }
                false -> Either.Left(Failure.DatabaseError)
            }
        }

        private fun <T, R> request(call: Call<T>, transform: (T) -> R, default: T): Either<Failure, R> {
            return try {
                val response = call.execute()
                when (response.isSuccessful) {
                    true -> Either.Right(transform((response.body() ?: default)))
                    false -> Either.Left(Failure.ServerError)
                }
            } catch (exception: Throwable) {
                Either.Left(Failure.ServerError)
            }
        }

    }


}