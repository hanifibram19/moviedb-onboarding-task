package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class GetAllFavoriteMovie
@Inject constructor(private val moviesRepository: MoviesRepository) : UseCase<List<Movie>, UseCase.None>() {
    override suspend fun run(params: None): Either<Failure, List<Movie>> = moviesRepository.getAllFavoriteMovie()
}