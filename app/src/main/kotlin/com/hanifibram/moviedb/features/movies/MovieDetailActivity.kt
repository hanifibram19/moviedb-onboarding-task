package com.hanifibram.moviedb.features.movies

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.hanifibram.moviedb.core.platform.BaseActivity
import com.hanifibram.moviedb.core.platform.BaseFragment

class MovieDetailActivity : BaseActivity() {

    override fun fragment(): BaseFragment =
            MovieDetailFragment.forInstance(intent.getParcelableExtra(INTENT_EXTRA_MOVIE))

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val INTENT_EXTRA_MOVIE = "hanifibram.com.INTENT_PARAM_MOVIE"

        fun callingIntent(context: Context, movie: Movie): Intent {
            val intent = Intent(context, MovieDetailActivity::class.java)
            intent.putExtra(INTENT_EXTRA_MOVIE, movie)
            return intent
        }
    }
}