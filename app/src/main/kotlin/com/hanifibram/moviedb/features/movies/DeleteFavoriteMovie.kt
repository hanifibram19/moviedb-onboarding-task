package com.hanifibram.moviedb.features.movies

import com.hanifibram.moviedb.core.exception.Failure
import com.hanifibram.moviedb.core.functional.Either
import com.hanifibram.moviedb.core.interactor.UseCase
import javax.inject.Inject

class DeleteFavoriteMovie
@Inject constructor(private val moviesRepository: MoviesRepository) : UseCase<UseCase.None, Int>() {
    override suspend fun run(params: Int): Either<Failure, None> = moviesRepository.deleteFavorite(params)

}