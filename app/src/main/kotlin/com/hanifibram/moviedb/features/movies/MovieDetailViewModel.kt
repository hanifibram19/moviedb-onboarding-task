package com.hanifibram.moviedb.features.movies

import android.arch.lifecycle.MutableLiveData
import com.hanifibram.moviedb.core.interactor.UseCase
import com.hanifibram.moviedb.core.platform.BaseViewModel
import javax.inject.Inject

class MovieDetailViewModel
@Inject constructor(private val getMovieDetail: GetMovieDetail,
                    private val getTrailers: GetTrailers,
                    private val playMovie: PlayMovie,
                    private val insertFavoriteMovie: InsertFavoriteMovie,
                    private val getFavoriteMovie: GetFavoriteMovie,
                    private val deleteFavoriteMovie: DeleteFavoriteMovie) : BaseViewModel() {

    var movieDetail: MutableLiveData<MovieDetail> = MutableLiveData()
    var trailers: MutableLiveData<List<Trailer>> = MutableLiveData()
    var movie: MutableLiveData<Movie> = MutableLiveData()
    var addFavoriteMovie: MutableLiveData<Boolean> = MutableLiveData()
    var removeFavoriteMovie: MutableLiveData<Boolean> = MutableLiveData()

    fun loadMoviesDetail(id: Int) =
            getMovieDetail(id) { it.fold(::handleFailure, ::handleMovieDetail) }

    fun loadTrailers(id: Int) = getTrailers(id) { it.fold(::handleFailure, ::handleTrailer) }

    fun insertFavoriteMovie(movie: Movie) =
            insertFavoriteMovie(movie) { it.fold(::handleFailure, ::handleInsertFavoriteMovie) }

    fun getFavoriteMovie(id: Int) =
            getFavoriteMovie(id) { it.fold(::handleFailure, ::handleFavoriteMovie) }

    fun deleteFavoriteMovie(id: Int) =
            deleteFavoriteMovie(id) { it.fold(::handleFailure, ::handleDeleteFavoriteMovie) }

    private fun handleMovieDetail(movieDetail: MovieDetail) {
        this.movieDetail.value = movieDetail
    }

    private fun handleTrailer(trailerWrapper: TrailerWrapper) {
        this.trailers.value = trailerWrapper.results
    }

    private fun handleInsertFavoriteMovie(none: UseCase.None) {
        addFavoriteMovie.value = true
    }

    private fun handleFavoriteMovie(movie: Movie) {
        this.movie.value = movie
    }

    private fun handleDeleteFavoriteMovie(none: UseCase.None) {
        this.removeFavoriteMovie.value = true
    }

    fun openYoutube(videoKey: String) {
        playMovie(videoKey)
    }


}